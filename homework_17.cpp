#include <iostream>

class Homework17
{
private:
	int x, y, z;
public:

	Homework17()
	{
		x = 1, y = 2, z = 3;
	}
	
	int GetX()
	{
		return x;
	}

	int GetY()
	{
		return y;
	}

	int GetZ()
	{
		return z;
	}
};

class Vector
{
public:
	Vector() : a(4), b(5), c(6)
	{}
	Vector(double _a, double _b, double _c) : a(_a), b(_b), c(_c)
	{}
	double L; // ����� �������

	void Exe()
	{
		L = sqrt(a * a + b * b + c * c);
	}

	void Show()
	{
		std::cout << '\n' << a << ' ' << b << ' ' << c << "\n" << "Length = " << L << "\n"; // ������ ����� �������
	}

	
private:
	double a;
	double b;
	double c;
};


int main()
{
	Homework17 temp;
	std::cout << temp.GetX() << "\n" << temp.GetY() << "\n" << temp.GetZ() << "\n";

	Vector v;
	v.Exe();
	v.Show();
}